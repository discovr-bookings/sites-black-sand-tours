import { createGlobalStyle } from 'styled-components';
import './swiper.min.css';

export default createGlobalStyle`
  
  html {
    box-sizing: border-box;
  }

  html, body {
    height: 100%;
  }

  body {
    margin: 0px;
    -webkit-font-smoothing: antialiased;
    color: ${props => props.theme.palette.text.primary};
    font-family: ${props => props.theme.font.body};
    font-weight: 400;
    font-size: 16px;
    line-height: 1.625;
    @media screen and (max-width: ${props => props.theme.responsive.medium}) {
      font-size: 13px
    }
    &.active {
      perspective: 1500px;
      overflow: hidden;
    }
  }

  a {
    color: ${props => props.theme.palette.secondary.main};
    text-decoration: none;
    transition: all 300ms ease-in-out;
    &:active,
    &:hover,
    &:focus {
      color: ${props => props.theme.palette.primary.main};
      outline:0;
    }
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    line-height: 1;
    margin: 0;
    font-weight: lighter;
    a {
      color: inherit !important;
      text-decoration: none !important;
      font-weight: inherit !important
    }
    strong {
      font-weight: normal;
      font-family: 'Nunito', sans-serif;
    }
  }

  h1 {
    font-family: ${props => props.theme.font.header};
    font-size: 3.75em;
    line-height: .85em;
    text-transform: uppercase;
    color: ${props => props.theme.palette.secondary.main};
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 3em;
    }
  }

  h2 {
    font-family: ${props => props.theme.font.header};
    font-size: 2.2em;
    line-height: 1.15em;
    text-transform: uppercase;
    margin-bottom: 1em;
    color: ${props => props.theme.palette.secondary.main};
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 2.1em;
    }
  }

  h3 {
    font-family: ${props => props.theme.font.header};
    font-size: 1.75em;
    line-height: 1.3em;
    color: ${props => props.theme.palette.text.primary};
    margin-bottom: 0.4em;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 1.5em;
    }
  }

  h4 {
    font-family: ${props => props.theme.font.header};
    font-size: 1.3em;
    line-height: 1.3em;
    color: ${props => props.theme.palette.text.primary};
    margin-bottom: 0.4em;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 1.15em;
    }
  }

  h5 {
    font-family: ${props => props.theme.font.cursive};
    font-size: 1.05em;
    line-height: 2.25em;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    color: ${props => props.theme.palette.text.grey};
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 0.9em;
    }
  }

  h6 {
    font-family: ${props => props.theme.font.header};
    font-size: 1.1em;
    line-height: 1.3em;
    color: ${props => props.theme.palette.text.primary};
    margin-bottom: 0.4em;
  }

  ul,
  ol {
    list-style-type: none;
    margin: 0;
    padding: 0
  }

  *, ::after, ::before {
    box-sizing: inherit;
  }

  ::selection,
  ::-moz-selection  {
    color: ${props => props.theme.palette.text.secondary};
    opacity: 1;
    background: ${props => props.theme.palette.primary.main};
  }

  /* Animations */
  .fadeInUp {
    animation-name: fadeInUp;
  }

  @keyframes fadeInUp {
    from {
      opacity: 0;
      transform: translate3d(0, 100%, 0);
    }
    to {
      opacity: 1;
      transform: translate3d(0, 0, 0);
    }
  }

  /* Weather */
  /* @font-face {
    font-family: 'weather';
    src: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.eot');
    src: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.eot?#iefix') format('embedded-opentype'),
      url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.woff') format('woff'),
      url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.ttf') format('truetype'),
      url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.svg#artill_clean_weather_iconsRg') format('svg');
    font-weight: normal;
    font-style: normal;
  }

  #weatherContainer{
    i {
      margin-right: 8px;
      &.weather-icon {
        color: ${props => props.theme.palette.primary.main};
        font-family: weather;
        font-size: 30px;
        font-weight: normal;
        font-style: normal;
        text-transform: none;
        &.icon-0:before { content: ":"; }
        &.icon-1:before { content: "p"; }
        &.icon-2:before { content: "S"; }
        &.icon-3:before { content: "Q"; }
        &.icon-4:before { content: "S"; }
        &.icon-5:before { content: "W"; }
        &.icon-6:before { content: "W"; }
        &.icon-7:before { content: "W"; }
        &.icon-8:before { content: "W"; }
        &.icon-9:before { content: "I"; }
        &.icon-10:before { content: "W"; }
        &.icon-11:before { content: "I"; }
        &.icon-12:before { content: "I"; }
        &.icon-13:before { content: "I"; }
        &.icon-14:before { content: "I"; }
        &.icon-15:before { content: "W"; }
        &.icon-16:before { content: "I"; }
        &.icon-17:before { content: "W"; }
        &.icon-18:before { content: "U"; }
        &.icon-19:before { content: "Z"; }
        &.icon-20:before { content: "Z"; }
        &.icon-21:before { content: "Z"; }
        &.icon-22:before { content: "Z"; }
        &.icon-23:before { content: "Z"; }
        &.icon-24:before { content: "E"; }
        &.icon-25:before { content: "E"; }
        &.icon-26:before { content: "3"; }
        &.icon-27:before { content: "a"; }
        &.icon-28:before { content: "A"; }
        &.icon-29:before { content: "a"; }
        &.icon-30:before { content: "A"; }
        &.icon-31:before { content: "6"; }
        &.icon-32:before { content: "1"; }
        &.icon-33:before { content: "6"; }
        &.icon-34:before { content: "1"; }
        &.icon-35:before { content: "W"; }
        &.icon-36:before { content: "1"; }
        &.icon-37:before { content: "S"; }
        &.icon-38:before { content: "S"; }
        &.icon-39:before { content: "S"; }
        &.icon-40:before { content: "M"; }
        &.icon-41:before { content: "W"; }
        &.icon-42:before { content: "I"; }
        &.icon-43:before { content: "W"; }
        &.icon-44:before { content: "a"; }
        &.icon-45:before { content: "S"; }
        &.icon-46:before { content: "U"; }
        &.icon-47:before { content: "S"; }
      }
    }
  } */
`;
