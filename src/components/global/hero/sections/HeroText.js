import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 20;
  margin-bottom: 0px;
  transform: translateX(-50%) translateY(-50%);
  text-align: center;
  width: 100%;
  h1 {
    font-family: ${props => props.theme.font.header};
    font-size: 3.75em;
    line-height: 0.85em;
    text-transform: uppercase;
    color: ${props => props.theme.palette.text.secondary};
    margin: 50px 0.5em 0px;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 3em;
    }
    span {
      font-size: 30px;
    }
  }
`;

const HeroText = props => {
  const { contentSection } = props;
  return (
    <Wrapper>
      <h1>
        {contentSection.name}
        <br />
        <span>{contentSection.bodyType.bodyType}</span>
      </h1>
    </Wrapper>
  );
};

export default HeroText;
