import React from 'react';
import Img from 'gatsby-image';
import styled from 'styled-components';

import config from '../../../../utils/siteConfig';

const Wrapper = styled(Img)`
  height: 80vh;
  min-height: 500px;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    min-height: 400px;
  }
  &:after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: ${props => props.theme.palette.background.overlay};
    content: '';
    z-index: 9;
  }
`;

const HeroLogo = props => {
  const { featuredImage } = props;
  return <Wrapper fluid={featuredImage.fluid} title={config.siteTitle} />;
};

export default HeroLogo;
