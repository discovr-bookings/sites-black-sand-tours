import styled from 'styled-components';

const HeroWrapper = styled.section`
  position: relative;
  height: 80vh;
  min-height: 500px;
  margin: 0px;
  overflow: hidden;
  z-index: 9;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    min-height: 400px;
  }
  &.thankyou {
    height: auto;
    min-height: 275px;
    @media screen and (max-width: ${props => props.theme.responsive.medium}) {
      min-height: 200px;
    }
  }
`;

export default HeroWrapper;
