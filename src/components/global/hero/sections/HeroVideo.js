import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  height: 80vh;
  min-height: 500px;
  @media screen and (max-width: 401px) {
    display: none;
  }
  &:after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: ${props => props.theme.palette.background.overlay};
    content: '';
    z-index: 9;
  }
`;

const HeroVideo = props => {
  const { contentSection } = props;
  return (
    <Wrapper>
      <video
        style={{ objectFit: 'cover', width: '100%', height: '80vh' }}
        poster={contentSection.image.fluid.srcWebp}
        id="background-video"
        playsInline
        loop
        muted
        autoPlay
      >
        <source
          src={contentSection.imageGallery[0].file.url}
          type="video/mp4"
        />
        <source
          src={contentSection.imageGallery[0].file.url}
          type="video/ogg"
        />
        <source
          src={contentSection.imageGallery[1].file.url}
          type="video/webm"
        />
        Your browser does not support the video tag.
      </video>
    </Wrapper>
  );
};
export default HeroVideo;
