import React from 'react';
import styled from 'styled-components';

import DownArrow from '../../../../assets/images/ui/icon-scroll-white.png';

const Wrapper = styled.div`
  cursor: pointer;
  position: absolute;
  bottom: 3.5em;
  left: 50%;
  background-image: url(${DownArrow});
  background-size: 27px 15px;
  padding-bottom: 30px;
  text-align: center;
  color: ${props => props.theme.palette.text.secondary};
  z-index: 20;
  width: 230px;
  margin-left: -120px;
  text-transform: uppercase;
  line-height: 1.7em;
  opacity: 0.6;
  font-family: ${props => props.theme.font.header};
  font-size: 0.75em;
  display: inline-block;
  letter-spacing: 0.25em;
  background-position: center bottom;
  background-repeat: no-repeat;
  overflow: hidden;
  &:hover {
    opacity: 1;
  }
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    font-size: 0.9em;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    text-indent: 101%;
    white-space: nowrap;
    margin-bottom: 1em;
    height: 1.7em;
    overflow: hidden;
    padding: 0px;
  }
`;

const HeroLogo = props => {
  const { pageTitle } = props;
  return <Wrapper>{pageTitle}</Wrapper>;
};

export default HeroLogo;
