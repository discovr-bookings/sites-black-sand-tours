import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';

import Logo from '../../../../assets/images/logos/black-sand-logo-light.svg';
import * as ROUTES from '../../../../constants/routes';
import config from '../../../../utils/siteConfig';

const Wrapper = styled(Link)`
  position: absolute;
  left: 50%;
  top: 72px;
  margin-left: -121px;
  text-indent: 101%;
  white-space: nowrap;
  display: block;
  background-size: 242px;
  height: 137px;
  width: 242px;
  overflow: hidden;
  background-repeat: no-repeat;
  background-position: center top;
  background-image: url(${Logo});
  z-index: 20;
  transition: all 300ms ease-in-out;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    top: 55px;
    background-size: 70%;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    background-size: 60%;
  }
  &:hover {
    opacity: 0.8;
  }
`;

const HeroLogo = () => (
  <Wrapper to={ROUTES.INDEX} title={config.siteTitle}>
    {config.siteTitle}
  </Wrapper>
);

export default HeroLogo;
