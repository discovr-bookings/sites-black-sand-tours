import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: fixed;
  height: 100vh;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
  z-index: 9999;
  background-color: rgba(0, 0, 0, 0.1);
  display: none;
  cursor: pointer;
  &.animate {
    display: block;
  }
`;

const Freezer = props => {
  const { menuClick, menuOpen } = props;
  return <Wrapper className={menuOpen ? 'animate' : ''} onClick={menuClick} />;
};

export default Freezer;
