import React, { Component } from 'react';

import { SubWrapper, SubInner, SubHorizontal, SubSelect } from './sections';

class SubMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { menu } = this.props;

    return (
      <SubWrapper>
        <SubInner>
          <SubHorizontal menu={menu} />
          <SubSelect menu={menu} />
        </SubInner>
      </SubWrapper>
    );
  }
}

export default SubMenu;
