import styled from 'styled-components';

const SubInner = styled.div`
  max-width: 1080px;
  margin-left: auto;
  margin-right: auto;
  box-sizing: content-box;
  text-align: center;
  padding: 2.6em 0px;
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    padding: 2.6em 0px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding: 21px;
  }
`;

export default SubInner;
