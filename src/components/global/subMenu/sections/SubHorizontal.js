import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';

const Wrapper = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    display: none;
  }
`;

const Item = styled.li`
  margin: 0px 0.8em;
`;

const MenuLink = styled(Link)`
  font-family: ${props => props.theme.font.header};
  font-size: 0.875em;
  line-height: 1.2em;
  letter-spacing: 0.15em;
  color: ${props => props.theme.palette.text.grey};
  text-transform: uppercase;
  &.active {
    color: ${props => props.theme.palette.secondary.main};
  }
`;

const SubHorizontal = props => {
  const { menu } = props;
  return (
    <Wrapper>
      {menu.page.map(i => (
        <Item key={i.id}>
          <MenuLink
            activeClassName="active"
            to={`${i.seoContent.slug}/`}
            title={i.seoContent.pageTitle}
          >
            {i.seoContent.pageTitle}
          </MenuLink>
        </Item>
      ))}
    </Wrapper>
  );
};

export default SubHorizontal;
