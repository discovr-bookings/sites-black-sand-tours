import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

const Wrapper = styled.ul`
  margin-right: 1em;
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    float: left;
  }
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    float: left;
    text-align: center;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-right: 0px;
  }
`;

const Item = styled.li`
  display: inline-block;
  &:not(:last-child) {
    margin-right: 1em;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    text-align: center;
    margin-right: 0px;
    display: block;
    padding: 0.75em 21px;
    &:first-child {
      padding-top: 2em;
    }
    &:not(:last-child) {
      margin-right: 0;
    }
  }
`;

const MenuLink = styled(Link)`
  font-family: ${props => props.theme.font.header};
  font-size: 0.75em;
  line-height: 1.2em;
  text-transform: uppercase;
  display: inline-block;
  position: relative;
  color: ${props => props.theme.palette.text.grey};
  letter-spacing: 0.25em;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    font-size: 0.9em;
  }
`;

const MenuLinkExternal = styled.a`
  font-family: ${props => props.theme.font.header};
  font-size: 0.75em;
  line-height: 1.2em;
  text-transform: uppercase;
  display: inline-block;
  position: relative;
  color: ${props => props.theme.palette.text.grey};
  letter-spacing: 0.25em;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    font-size: 0.9em;
  }
`;

const FooterMenu = () => (
  <StaticQuery
    query={graphql`
      query {
        contentfulMenus(id: { eq: "65f9fb4f-7df4-5081-8515-e26bd992ac93" }) {
          id
          page {
            id
            menuTitle
            seoContent {
              pageTitle
              slug
            }
          }
        }
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          rezdyBookingLink
        }
      }
    `}
    render={data => {
      const { page } = data.contentfulMenus;
      const { rezdyBookingLink } = data.contentfulCompanyInformation;
      return (
        <Wrapper>
          {page.map(i => (
            <Item key={i.id}>
              <MenuLink
                to={`${i.seoContent.slug}/`}
                title={i.seoContent.pageTitle}
              >
                {i.menuTitle}
              </MenuLink>
            </Item>
          ))}
          <Item>
            <MenuLinkExternal
              href={rezdyBookingLink}
              title="Book your Black Sand Adventures Tour"
              target="_blank"
              rel="noopener noreferrer"
            >
              Booking
            </MenuLinkExternal>
          </Item>
        </Wrapper>
      );
    }}
  />
);

export default FooterMenu;
