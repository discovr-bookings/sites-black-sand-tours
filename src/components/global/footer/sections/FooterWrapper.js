import styled from 'styled-components';

const FooterWrapper = styled.footer`
  margin-top: 0;
  background: ${props => props.theme.palette.background.light};
`;

export default FooterWrapper;
