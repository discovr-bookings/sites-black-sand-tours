import React from 'react';
import styled from 'styled-components';
import { FooterTime } from '.';

const Wrapper = styled.ul`
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    float: right;
  }
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    text-align: center;
    padding-bottom: 0px;
    padding-top: 3em;
  }
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    padding-bottom: 0px;
    margin: 0px auto;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding-top: 1em;
    padding-bottom: 2em;
  }
`;

const Item = styled.li`
  font-family: ${props => props.theme.font.header};
  font-size: 0.75em;
  line-height: 1.2em;
  text-transform: uppercase;
  display: inline-block;
  position: relative;
  color: ${props => props.theme.palette.text.grey};
  letter-spacing: 0.25em;
  &:not(:last-child) {
    margin-right: 2em;
  }
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    font-size: 0.9em;
  }
`;

const FooterWidgets = () => (
  <Wrapper>
    <Item>
      <FooterTime />
    </Item>
  </Wrapper>
);

export default FooterWidgets;
