import FooterWrapper from './FooterWrapper';
import FooterTop from './FooterTop';
import FooterTopInner from './FooterTopInner';
import FooterMenu from './FooterMenu';
import FooterWidgets from './FooterWidgets';
import FooterTime from './FooterTime';
import FooterBottom from './FooterBottom';
import FooterBottomInner from './FooterBottomInner';
import FooterCopy from './FooterCopy';
import FooterSocial from './FooterSocial';
import FooterLegal from './FooterLegal';

export {
  FooterWrapper,
  FooterTop,
  FooterTopInner,
  FooterMenu,
  FooterWidgets,
  FooterTime,
  FooterBottom,
  FooterBottomInner,
  FooterCopy,
  FooterSocial,
  FooterLegal
};
