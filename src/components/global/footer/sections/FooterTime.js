import React, { Component } from 'react';
import { tz } from 'moment-timezone';

class FooterTime extends Component {
  constructor() {
    super();
    this.state = {
      timezone: 'Pacific/Auckland'
    };
  }

  render() {
    const { timezone } = this.state;
    return (
      <span id={timezone}>
        Current time is: {tz(timezone).format('H:mm a')}
      </span>
    );
  }
}
export default FooterTime;
