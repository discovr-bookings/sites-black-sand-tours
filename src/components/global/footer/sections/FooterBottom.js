import styled from 'styled-components';

const FooterBottom = styled.div`
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    width: 100%;
    display: inline-block;
    margin: 0px auto;
  }
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    padding: 1.5em 0px;
  }
`;

export default FooterBottom;
