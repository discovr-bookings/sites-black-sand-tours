import React from 'react';

import {
  FooterWrapper,
  FooterTop,
  FooterTopInner,
  FooterMenu,
  FooterWidgets,
  FooterBottom,
  FooterBottomInner,
  FooterCopy,
  FooterSocial,
  FooterLegal
} from './sections';

const Footer = () => (
  <FooterWrapper>
    <FooterTop>
      <FooterTopInner>
        <FooterMenu />
        <FooterWidgets />
      </FooterTopInner>
    </FooterTop>
    <FooterBottom>
      <FooterBottomInner>
        <FooterCopy />
        <div>
          <FooterSocial />
          <FooterLegal />
        </div>
      </FooterBottomInner>
    </FooterBottom>
  </FooterWrapper>
);

export default Footer;
