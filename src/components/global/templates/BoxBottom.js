import React from 'react';
import { Link } from 'gatsby';
import Img from 'gatsby-image';
import styled from 'styled-components';

const Wrapper = styled.div`
  z-index: 1;
  position: relative;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin: 50px 0 0;
    position: relative;
  }
`;

const Inner = styled.div`
  display: flex;
  max-width: 1080px;
  margin-left: auto;
  margin-right: auto;
  box-sizing: content-box;
  padding-right: 42px;
  padding-left: 42px;
  z-index: 1;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding-right: 21px;
    padding-left: 21px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    flex-direction: column-reverse;
  }
  &.reversed {
    flex-direction: row-reverse;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      flex-direction: column-reverse;
    }
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

const One = styled.div`
  float: left;
  display: block;
  margin-right: 5.62516%;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    float: left;
    display: block;
    padding-top: 60px;
    margin-right: 5.62516%;
    margin-left: 5.62516%;
    width: 47.1874%;
  }
  &.reversed {
    @media screen and (min-width: ${props => props.theme.responsive.small}) {
      float: left;
      display: block;
      width: 38.3853%;
      margin-right: 0;
      margin-left: 0;
    }
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-bottom: 21px;
    text-align: center;
  }
  a {
    font-family: ${props => props.theme.font.header};
    line-height: 1.2em;
    text-transform: uppercase;
    display: inline-block;
    position: relative;
    font-size: 90%;
    color: ${props => props.theme.palette.text.black};
    letter-spacing: 0.25em;
    padding-bottom: 0.6em;
    &:hover {
      color: ${props => props.theme.palette.primary.main};
    }
    &:after {
      content: '';
      position: absolute;
      right: 0px;
      bottom: 0px;
      left: 0px;
      height: 1px;
      background-color: ${props => props.theme.palette.text.black};
    }
  }
`;

const Two = styled.div`
  position: relative;
  float: left;
  display: block;
  margin-right: 0px;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    margin-right: 0px;
    float: left;
    display: block;
    width: 47.1874%;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-bottom: 21px;
  }
  .gatsby-image-wrapper {
    max-height: 325px;
    height: 325px;
    @media screen and (max-width: ${props =>
        props.theme.responsive.medium}) and (min-width: ${props =>
        props.theme.responsive.small}) {
      max-height: 300px;
    }
    img {
      width: 100%;
      display: block;
    }
  }
  &.reversed {
    .gatsby-image-wrapper {
      max-height: 100%;
      height: auto;
      @media screen and (max-width: ${props =>
          props.theme.responsive.medium}) and (min-width: ${props =>
          props.theme.responsive.small}) {
        max-height: 100%;
      }
      img {
        width: 100%;
        display: block;
      }
    }
    @media screen and (min-width: ${props => props.theme.responsive.small}) {
      float: left;
      display: block;
      margin-right: 5.62516%;
      margin-left: 8.8021%;
      width: 47.1874%;
    }
  }
`;

const BoxBottom = props => {
  const { contentSection, reversed } = props;
  return (
    <Wrapper>
      <Inner className={reversed ? 'reversed' : ''}>
        <One className={reversed ? 'reversed' : ''}>
          <h5>#blacksandadventures</h5>
          <h2>{contentSection.name}</h2>
          <div
            dangerouslySetInnerHTML={{
              __html: contentSection.bodyType.childMarkdownRemark.html
            }}
          />
          {contentSection.link !== null && (
            <Link
              title={contentSection.link.seoContent.pageTitle}
              to={`${contentSection.link.seoContent.slug}/`}
            >
              {contentSection.link.seoContent.pageTitle}
            </Link>
          )}
        </One>
        <Two className={reversed ? 'reversed' : ''}>
          <Img
            fluid={contentSection.image.fluid}
            title={contentSection.image.title}
            alt={contentSection.image.description}
          />
        </Two>
      </Inner>
    </Wrapper>
  );
};

export default BoxBottom;
