import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  text-align: center;
  position: relative;
`;

const HashTag = styled.h5`
  text-align: center;
`;

const Title = styled.h2`
  text-align: center;
`;

const Paragraph = styled.div`
  max-width: 80%;
  margin: 0px auto;
`;

const PageTitle = props => {
  const { titleContent, location, terms } = props;
  const { pageTitle, description } = titleContent;
  return (
    <Wrapper>
      {!terms && <HashTag>#travellikealocal</HashTag>}
      <Title>{pageTitle}</Title>
      {location.pathname === '/where-we-go/' && (
        <Paragraph>
          <div
            dangerouslySetInnerHTML={{
              __html: description.childMarkdownRemark.html
            }}
          />
        </Paragraph>
      )}
    </Wrapper>
  );
};

export default PageTitle;
