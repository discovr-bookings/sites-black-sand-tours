import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: relative;
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    margin: 100px 0px;
  }
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    margin: 50px 0px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin: 35px 0px;
  }
  &.two-rows {
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      background-color: ${props => props.theme.palette.background.middle};
      padding: 25px 0px;
      margin: 0px;
    }
  }
  &.first-box {
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      margin: 0;
    }
  }
  &.no-bg {
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      padding: 25px 0px;
    }
  }
  &.dark {
    position: relative;
    overflow: hidden;
    margin: 0px;
    &:after {
      position: absolute;
      width: calc(100% - 28px);
      height: 150vh;
      top: 0px;
      right: 14px;
      bottom: 0px;
      left: 14px;
      content: '';
      background-color: ${props => props.theme.palette.background.dark};
      @media screen and (max-width: ${props => props.theme.responsive.medium}) {
        width: 100%;
        left: 0;
        right: 0;
      }
    }
  }
  &.primary {
    position: relative;
    overflow: hidden;
    margin: 0px;
    &:after {
      position: absolute;
      width: calc(100% - 28px);
      height: 150vh;
      top: 0px;
      right: 14px;
      bottom: 0px;
      left: 14px;
      content: '';
      background-color: ${props => props.theme.palette.primary.main};
      @media screen and (max-width: ${props => props.theme.responsive.medium}) {
        width: 100%;
        left: 0;
        right: 0;
      }
    }
  }
  &.grey {
    position: relative;
    margin: 0px;
    overflow: hidden;
    &:after {
      position: absolute;
      width: calc(100% - 28px);
      height: 250vh;
      top: 0px;
      right: 14px;
      bottom: 0px;
      left: 14px;
      content: '';
      background-color: ${props => props.theme.palette.background.middle};
      @media screen and (max-width: ${props => props.theme.responsive.medium}) {
        width: 100%;
        left: 0;
        right: 0;
      }
    }
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

const Box = props => {
  const { TwoRows, FirstBox, Dark, Primary, Grey, noBG, children } = props;
  return (
    <Wrapper
      className={`${TwoRows ? 'two-rows' : ''} ${FirstBox ? 'first-box' : ''} ${
        Primary ? 'primary' : ''
      } ${Grey ? 'grey' : ''} ${Dark ? 'dark' : ''} ${noBG ? 'no-bg' : ''}`}
    >
      {children}
    </Wrapper>
  );
};

export default Box;
