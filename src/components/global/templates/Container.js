import styled from 'styled-components';

const Container = styled.div`
  transform: translate3d(0px, 0px, 0px);
  position: relative;
  z-index: 8;
  background-color: rgb(255, 255, 255);
  transition: transform 0.4s ease;
  &.active {
    position: absolute;
    cursor: pointer;
    height: 100%;
    width: 100%;
    backface-visibility: hidden;
    overflow: hidden;
    transition-delay: 400ms;
    @media screen and (min-width: ${props => props.theme.responsive.medium}) {
      transform: translateZ(0px) translateX(10%) rotateY(-50deg);
    }
    @media screen and (max-width: ${props =>
        props.theme.responsive.medium}) and (min-width: ${props =>
        props.theme.responsive.small}) {
      transform: translateZ(0px) translateX(30%) rotateY(-50deg);
    }
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      transform: translateZ(0px) translateX(50%) rotateY(-70deg);
    }
  }
`;

export default Container;
