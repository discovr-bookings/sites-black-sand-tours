import React from 'react';
import { Helmet } from 'react-helmet';
import config from '../../../utils/siteConfig';

class SeoDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { seoContent, menuOpen } = this.props;

    const { pageTitle, slug, description, featuredImage } = seoContent;

    const pageUrl = `${config.siteUrl}/${slug}/`;
    const pageImage = config.siteUrl + featuredImage.fluid.src;
 
    // Default Website Schema
    const schemaOrgJSONLD = [
      {
        '@context': 'http://schema.org',
        '@type': 'WebSite',
        url: config.siteUrl,
        name: config.siteTitle
      }
    ];

    schemaOrgJSONLD.push({
      '@context': 'http://schema.org',
      '@type': 'WebPage',
      url: pageUrl,
      name: pageTitle
    });

    return (
      <Helmet>
        <html lang="en" />
        <body className={menuOpen ? 'active' : ''} />
        {/* General tags */}
        <title>
          {pageTitle} | {config.siteTitle}
        </title>
        <meta name="image" content={pageImage} />
        <meta name="description" content={description.description} />

        {/* Schema.org tags */}
        <script type="application/ld+json">
          {JSON.stringify(schemaOrgJSONLD)}
        </script>

        {/* OpenGraph tags */}
        <meta property="og:title" content={pageTitle} />
        <meta property="og:url" content={pageUrl} />
        <meta property="og:image" content={pageImage} />
        <meta property="og:description" content={description.description} />

        {/* Twitter Card tags */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:creator" content={config.twitterHandle} />
        <meta name="twitter:title" content={pageTitle} />
        <meta name="twitter:image" content={pageImage} />
        <meta name="twitter:description" content={description.description} />
      </Helmet>
    );
  }
}

export default SeoDetails;
