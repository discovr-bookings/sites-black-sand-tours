import React from 'react';

import {
  NavWrapper,
  NavLogo,
  MenuWrapper,
  Menu,
  MenuSpecial,
  MenuFooter
} from './sections';

const Header = props => {
  const { menuOpen } = props;
  return (
    <NavWrapper>
      <NavLogo />
      <MenuWrapper>
        <Menu menuOpen={menuOpen} />
        <MenuSpecial menuOpen={menuOpen} />
        <MenuFooter menuOpen={menuOpen} />
      </MenuWrapper>
    </NavWrapper>
  );
};

export default Header;
