import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

const Wrapper = styled.ul`
  opacity: 0;
  transition: opacity 0s ease 0s, all 0.2s ease 0s;
  display: flex;
  justify-content: space-evenly;
  &.active {
    opacity: 1;
  }
`;

const Item = styled.li`
  opacity: 0;
  transition: opacity 400ms ease-in-out;
  transition-delay: 250ms;
  &.active {
    &:nth-child(1) {
      animation: fadeInUp 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.6s 1
        normal forwards running;
    }
    &:nth-child(2) {
      animation: fadeInUp 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.65s 1
        normal forwards running;
    }
    &:nth-child(3) {
      animation: fadeInUp 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.7s 1
        normal forwards running;
    }
    &:nth-child(4) {
      animation: fadeInUp 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.75s 1
        normal forwards running;
    }
  }
`;

const MenuLink = styled(Link)`
  color: ${props => props.theme.palette.text.grey};
  font-family: ${props => props.theme.font.header};
  text-transform: uppercase;
  font-size: 0.8em;
  line-height: 2em;
  display: block;
`;

const MenuFooter = props => (
  <StaticQuery
    query={graphql`
      query {
        contentfulMenus(id: { eq: "9f8513c7-689c-5ff0-b684-a671370bf408" }) {
          id
          page {
            id
            menuTitle
            seoContent {
              slug
              pageTitle
            }
          }
        }
      }
    `}
    render={data => {
      const { page } = data.contentfulMenus;
      const { menuOpen } = props;
      return (
        <Wrapper className={menuOpen ? 'active' : ''}>
          {page.map(i => (
            <Item key={i.id} className={menuOpen ? 'active' : ''}>
              <MenuLink
                to={`${i.seoContent.slug}/`}
                title={i.seoContent.pageTitle}
              >
                {i.menuTitle}
              </MenuLink>
            </Item>
          ))}
        </Wrapper>
      );
    }}
  />
);

export default MenuFooter;
