import styled from 'styled-components';

const MenuWrapper = styled.div`
  max-width: 320px;
  height: calc(100% - 70px);
  width: 40%;
  overflow: hidden auto;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    width: 75%;
    height: 100%;
  }
`;

export default MenuWrapper;
