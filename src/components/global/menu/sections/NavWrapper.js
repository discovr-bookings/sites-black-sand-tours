import styled from 'styled-components';

const NavWrapper = styled.nav`
  position: absolute;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
  overflow: hidden;
  margin: 5%;
  z-index: 5;
`;

export default NavWrapper;
