import styled from 'styled-components';

const HeaderLeft = styled.ul`
  float: left;
  position: relative;
  z-index: 20;
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

export default HeaderLeft;
