import React from 'react';
import styled from 'styled-components';
import {
  FacebookShareButton,
  TwitterShareButton,
  PinterestShareButton,
  EmailShareButton,
  FacebookIcon,
  TwitterIcon,
  PinterestIcon,
  EmailIcon
} from 'react-share';
import config from '../../../../utils/siteConfig';

const Wrapper = styled.div`
  position: absolute;
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  border-bottom: 1px solid rgba(0, 0, 0, 0.3);
  top: 70px;
  right: 0;
  width: auto;
  background: ${props => props.theme.palette.primary.main};
  z-index: 100;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    top: 55px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    width: 100%;
  }
  svg {
    margin: 0 !important;
    path {
      fill: ${props => props.theme.palette.text.secondary} !important;
    }
  }
`;

const ShareLinks = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-wrap: none;
  width: auto;
  height: auto;
  > button {
    outline: 0;
    cursor: pointer;
    width: 100%;
    height: 52px;
    width: 52px;
    border-left: 1px solid rgba(0, 0, 0, 0.3);
    display: flex;
    justify-content: center;
    align-items: center;
    @media screen and (max-width: ${props => props.theme.responsive.medium}) {
      height: 42px;
      width: 42px;
    }
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      height: 71px;
      width: 55px;
    }
    svg {
      height: 40px;
      width: 40px;
      rect {
        fill: transparent;
      }
      path {
        transition: fill 0.2s ease-in-out;
      }
    }
    &:hover {
      background: ${props => props.theme.palette.primary.hover};
    }
  }
`;

const ShareIcons = props => {
  const { seoContent } = props;
  const { slug, pageTitle, featuredImage } = seoContent;

  const shareURl = config.siteUrl + `/` + slug;
  const media = featuredImage.fluid.src;

  return (
    <Wrapper className="shareButtons">
      <ShareLinks>
        <FacebookShareButton
          url={shareURl}
          quote={`${pageTitle} - ${config.siteTitle}`}
        >
          <FacebookIcon size={40} />
        </FacebookShareButton>
        <TwitterShareButton url={shareURl}>
          <TwitterIcon size={40} />
        </TwitterShareButton>
        <PinterestShareButton url={shareURl} media={media}>
          <PinterestIcon size={40} />
        </PinterestShareButton>
        <EmailShareButton
          url={shareURl}
          subject={`Thought you may like this by ${
            config.siteTitle
          } - ${pageTitle}`}
          body={shareURl}
        >
          <EmailIcon size={40} />
        </EmailShareButton>
      </ShareLinks>
    </Wrapper>
  );
};

export default ShareIcons;
