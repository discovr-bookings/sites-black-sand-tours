import React from 'react';
import styled from 'styled-components';

import { ShareIcon } from '../../../../assets/images/siteIcons';
import ModalShare from './ModalShare';

const Wrapper = styled.div`
  display: block;
  float: right;
  padding: 0px 42px;
  cursor: pointer;
  transition: all 300ms ease-in-out;
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    padding: 0px 30px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    height: 55px;
    width: 55px;
    padding: 0px;
    &.active {
      border-left: 1px solid rgba(0, 0, 0, 0.3);
    }
  }
  svg {
    height: 20px;
    width: 22px;
    margin: 0 17px -5px 0;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      margin: 17px;
    }
    path {
      transition: fill 300ms ease-in-out;
      fill: ${props => props.theme.palette.text.secondary};
    }
  }
  .shareButtons {
    transform: scaleY(0);
    transform-origin: left top 0px;
    opacity: 0;
    visibility: hidden;
    transition: all 300ms ease-in-out;
  }
  &.active {
    svg {
      path {
        fill: ${props => props.theme.palette.text.primary};
      }
    }
    span {
      color: ${props => props.theme.palette.text.primary};
    }
  }
  &.showShare {
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      background: ${props => props.theme.palette.primary.main};
      svg {
        path {
          fill: ${props => props.theme.palette.text.secondary};
        }
      }
    }
    .shareButtons {
      @media screen and (max-width: ${props => props.theme.responsive.small}) {
        transform: scaleY(1);
        opacity: 1;
        visibility: visible;
      }
    }
  }
  &:hover {
    svg {
      path {
        fill: ${props => props.theme.palette.primary.main};
      }
    }
    span {
      color: ${props => props.theme.palette.primary.main};
    }
    .shareButtons {
      @media screen and (min-width: ${props => props.theme.responsive.small}) {
        transform: scaleY(1);
        opacity: 1;
        visibility: visible;
      }
    }
    &.showShare {
      @media screen and (max-width: ${props => props.theme.responsive.small}) {
        svg {
          path {
            fill: ${props => props.theme.palette.text.secondary};
          }
        }
      }
    }
  }
`;

const Text = styled.span`
  font-family: ${props => props.theme.font.header};
  color: ${props => props.theme.palette.text.secondary};
  font-weight: 700;
  font-size: 0.8em;
  line-height: 70px;
  text-transform: uppercase;
  display: inline-block;
  position: relative;
  letter-spacing: 0.25em;
  transition: all 300ms ease-in-out;
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    line-height: 55px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    display: none;
  }
`;

const ShareTrigger = props => {
  const { scrolling, shareClick, sharing, seoContent } = props;
  return (
    <Wrapper
      onClick={shareClick}
      className={`${scrolling ? 'active' : ''} ${sharing ? 'showShare' : ''}`}
    >
      <ShareIcon />
      <Text>Share</Text>
      <ModalShare seoContent={seoContent} />
    </Wrapper>
  );
};

export default ShareTrigger;
