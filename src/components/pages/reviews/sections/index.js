import ReviewsWrapper from './ReviewsWrapper';
import ReviewsInner from './ReviewsInner';
import ReviewsLeft from './ReviewsLeft';
import ReviewsRight from './ReviewsRight';

export { ReviewsWrapper, ReviewsInner, ReviewsLeft, ReviewsRight };
