import styled from 'styled-components';

const ReviewInner = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    flex-direction: column-reverse;
  }
`;

export default ReviewInner;
