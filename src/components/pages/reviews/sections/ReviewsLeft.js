import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

import tripAdvisorLogo from '../../../../assets/images/ui/tripAdvisor_logo.png';

const Wrapper = styled.div`
  margin: 40px 20px 40px 0;
  width: 50%;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin: 0;
    width: 100%;
    text-align: center;
  }
`;

const Top = styled.div`
  h3 {
    color: ${props => props.theme.palette.text.black};
  }
  img {
    width: 150px;
  }
  small {
    color: ${props => props.theme.palette.text.grey};
    text-decoration: underline;
  }
`;

const Middle = styled.div`
  padding: 30px 0 15px;
  margin: 10px 0px;
  h4 {
    color: ${props => props.theme.palette.text.black};
    margin: 0;
  }
  p {
    color: #207b6c;
    text-transform: uppercase;
    font-weight: 600;
  }
`;

const Bottom = styled.div`
  a {
    font-family: ${props => props.theme.font.header};
    line-height: 1.2em;
    text-transform: uppercase;
    display: inline-block;
    position: relative;
    font-size: 90%;
    color: ${props => props.theme.palette.text.black};
    letter-spacing: 0.25em;
    padding-bottom: 0.6em;
    &:hover {
      color: ${props => props.theme.palette.primary.main};
    }
    &:first-child {
      margin-right: 15px;
    }
    &:after {
      content: '';
      position: absolute;
      right: 0px;
      bottom: 0px;
      left: 0px;
      height: 1px;
      background-color: ${props => props.theme.palette.text.black};
    }
  }
  img {
    margin: 40px 0 20px;
    display: block;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      margin: 40px auto 10px;
    }
  }
  small {
    color: ${props => props.theme.palette.text.grey};
  }
`;

const ReviewsLeft = props => {
  const { tripAdvisorData } = props;
  return (
    <Wrapper>
      <Top>
        <h3>TripAdvisor Traveller Rating</h3>
        <img
          src={tripAdvisorData.rating_image_url.replace(
            /^http:\/\//i,
            'https://'
          )}
          alt={tripAdvisorData.ranking_data.ranking_string}
        />
        <br />
        <small>
          based on&nbsp;
          {tripAdvisorData.num_reviews}
          &nbsp;traveller reviews
        </small>
      </Top>
      <Middle>
        <h4>TripAdvisor Ranking</h4>
        <p>{tripAdvisorData.ranking_data.ranking_string}</p>
      </Middle>
      <Bottom>
        <a
          href={tripAdvisorData.web_url}
          target="_blank"
          rel="noopener noreferrer"
          title={`More ${tripAdvisorData.name} Reviews`}
        >
          More Reviews
        </a>
        <a
          href={tripAdvisorData.write_review}
          target="_blank"
          rel="noopener noreferrer"
          title={`Write a review for ${tripAdvisorData.name}`}
        >
          Write Review
        </a>
        <img src={tripAdvisorLogo} alt="TripAdvisor Logo" />
        <small>© {moment().year()} TripAdvisor LLC</small>
      </Bottom>
    </Wrapper>
  );
};

export default ReviewsLeft;
