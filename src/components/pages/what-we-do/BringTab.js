import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.ul`
  margin-top: 20px;
  li {
    padding: 15px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    background: ${props => props.theme.palette.border.light};
    border-radius: 6px;
    margin-bottom: 1em;
    overflow: hidden;
    @media screen and (max-width: 47.9375em) {
      flex-direction: column;
    }
    div {
      padding: 15px 15px 15px 30px;
      height: 40px;
      width: 30px;
      color: rgb(160, 163, 171);
      display: inherit;
      -webkit-box-flex: 1;
      flex: 1 1 0%;
      img {
        height: 40px;
        width: 40px;
        filter: brightness(80%);
      }
      @media screen and (max-width: 47.9375em) {
        display: none;
      }
    }
    p {
      padding: 0 15px;
      width: 100%;
      margin: 0px;
    }
  }
`;

const BringTab = props => {
  const { contentSection } = props;
  return (
    <Wrapper>
      {contentSection.map(i => (
        <li key={i.text}>
          <div>
            <img src={i.icon} alt={i.text} />
          </div>
          <p>{i.text}</p>
        </li>
      ))}
    </Wrapper>
  );
};

export default BringTab;
