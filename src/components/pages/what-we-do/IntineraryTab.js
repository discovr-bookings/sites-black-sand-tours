import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.ul`
  margin-top: 20px;
  position: relative;
  li {
    position: relative;
    display: block;
    background: ${props => props.theme.palette.border.light};
    border-radius: 6px;
    margin-bottom: 1em;
    padding: 5px 15px;
    overflow: hidden;
  }
`;

const IntineraryTab = props => {
  const { contentSection } = props;
  return (
    <Wrapper>
      {contentSection.map(i => (
        <li key={i.title}>
          <p>{i.title}</p>
        </li>
      ))}
    </Wrapper>
  );
};

export default IntineraryTab;
