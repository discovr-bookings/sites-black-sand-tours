import styled from 'styled-components';

const WhereWrapper = styled.ul`
  position: relative;
  max-width: 1080px;
  text-align: center;
  margin: 0 auto;
  padding: 2em 0 0;
  list-style: none;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
  z-index: 1;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    grid-template-columns: 1fr;
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

export default WhereWrapper;
