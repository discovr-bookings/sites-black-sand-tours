import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  text-align: center;
  position: relative;
`;

const HashTag = styled.h5`
  text-align: center;
`;

const Title = styled.h2`
  text-align: center;
`;

const GoodToKnowTitle = () => (
  <Wrapper>
    <HashTag>#travellikealocal</HashTag>
    <Title>Good to know</Title>
  </Wrapper>
);

export default GoodToKnowTitle;
