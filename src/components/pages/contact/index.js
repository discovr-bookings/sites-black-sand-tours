import React from 'react';

import { ContactWrapper, ContactLeft, ContactRight } from './sections';

const ContactUs = () => (
  <ContactWrapper>
    <ContactLeft />
    <ContactRight />
  </ContactWrapper>
);
export default ContactUs;
