import React from 'react';
import styled from 'styled-components';
import { ContactSocial, ContactDetails } from '.';

const Wrapper = styled.div`
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    margin-right: 10%;
  }
  h6 {
    color: ${props => props.theme.palette.text.black};
    margin: 20px 0 10px;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      text-align: center;
    }
  }
`;

const ContactLeft = () => (
  <Wrapper>
    <ContactDetails />
    <h6>Stay in touch with us:</h6>
    <ContactSocial />
  </Wrapper>
);

export default ContactLeft;
