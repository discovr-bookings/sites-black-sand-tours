import React from 'react';
import styled from 'styled-components';
import { ContactForm } from '.';

const Wrapper = styled.div`
  width: 100%;
`;

const Title = styled.h2`
  color: ${props => props.theme.palette.text.black};
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    text-align: center;
  }
`;

const ContactRight = () => (
  <Wrapper>
    <Title>Get in touch</Title>
    <ContactForm />
  </Wrapper>
);

export default ContactRight;
