import styled from 'styled-components';

const ContactWrapper = styled.div`
  display: flex;
  z-index: 5;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-top: 0;
    flex-direction: column-reverse;
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

export default ContactWrapper;
