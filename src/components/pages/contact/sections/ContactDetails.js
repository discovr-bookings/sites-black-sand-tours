import React from 'react';
import { StaticQuery, graphql, Link } from 'gatsby';
import styled from 'styled-components';

import * as ROUTES from '../../../../constants/routes';

const Wrapper = styled.div`
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-top: 50px;
    text-align: center;
  }
`;

const ContactDetails = () => (
  <StaticQuery
    query={graphql`
      query {
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          companyName
          address
          city
          state
          postcode
          country
          email
          phone
          url
        }
      }
    `}
    render={data => {
      const {
        companyName,
        address,
        city,
        state,
        postcode,
        country,
        email,
        phone,
        url
      } = data.contentfulCompanyInformation;
      return (
        <Wrapper>
          <p>
            <strong>{companyName}&nbsp;</strong>
            <br />
            {address},&nbsp;{city}.
            <br />
            {state}&nbsp;{postcode}.&nbsp;{country}.<br /><br />
            <a href={`tel:${phone}`}>{phone}</a>
            <br />
            <a href={`mailto:${email}`}>{email}</a>
            <br />
            <Link to={ROUTES.INDEX}>{url.replace(/^https:\/\//i, '')}</Link>
          </p>
        </Wrapper>
      );
    }}
  />
);

export default ContactDetails;
