import ContactWrapper from './ContactWrapper';
import ContactLeft from './ContactLeft';
import ContactRight from './ContactRight';
import ContactDetails from './ContactDetails';
import ContactSocial from './ContactSocial';
import ContactForm from './ContactForm';

export {
  ContactWrapper,
  ContactLeft,
  ContactRight,
  ContactDetails,
  ContactSocial,
  ContactForm
};
