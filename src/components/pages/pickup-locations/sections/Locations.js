import React from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';

const Wrapper = styled.div`
  margin: 50px 0 30px;
`;

const PickupItem = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 50px;
  align-items: center;
  margin-bottom: 50px;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    grid-template-columns: 1fr;
    grid-gap: 20px;
    text-align: center;
  }
`;

const Left = styled.div``;
const Right = styled.div`
  h4 {
    color: ${props => props.theme.palette.text.black};
  }
  a {
    font-family: ${props => props.theme.font.header};
    line-height: 1.2em;
    text-transform: uppercase;
    display: inline-block;
    position: relative;
    font-size: 90%;
    color: ${props => props.theme.palette.text.black};
    letter-spacing: 0.25em;
    padding-bottom: 0.6em;
    &:hover {
      color: ${props => props.theme.palette.primary.main};
    }
    &:after {
      content: '';
      position: absolute;
      right: 0px;
      bottom: 0px;
      left: 0px;
      height: 1px;
      background-color: ${props => props.theme.palette.text.black};
    }
  }
`;

const Locations = props => {
  const { pickupLocations } = props;
  return (
    <Wrapper>
      {pickupLocations.map(({ node: pickupLocation }) => {
        const {
          id,
          name,
          mapLink,
          locationImage,
          description
        } = pickupLocation;
        return (
          <PickupItem key={id}>
            <Left>
              <Img
                fluid={locationImage.fluid}
                alt={locationImage.description}
              />
            </Left>
            <Right>
              <h4>{name}</h4>
              <p>{description.description}</p>
              <a href={mapLink} target="_blank" rel="noopener noreferrer">
                Get Directions
              </a>
            </Right>
          </PickupItem>
        );
      })}
    </Wrapper>
  );
};

export default Locations;
