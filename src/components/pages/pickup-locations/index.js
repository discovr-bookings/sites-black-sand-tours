import React, { Fragment } from 'react';
import { StaticQuery, graphql } from 'gatsby';

import { Locations, GoogleMaps } from './sections';

const PickupLocations = () => (
  <StaticQuery
    query={graphql`
      query {
        allContentfulPickupLocations {
          edges {
            node {
              id
              name
              description {
                description
              }
              location {
                lon
                lat
              }
              mapLink
              locationImage {
                id
                title
                description
                fluid(maxWidth: 800) {
                  ...GatsbyContentfulFluid_withWebp_noBase64
                }
              }
            }
          }
        }
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          coordinates {
            lon
            lat
          }
        }
      }
    `}
    render={data => {
      const pickupLocations = data.allContentfulPickupLocations.edges;
      const companyInformation = data.contentfulCompanyInformation;
      return (
        <Fragment>
          <GoogleMaps
            pickupLocations={pickupLocations}
            companyInformation={companyInformation}
          />
          <Locations pickupLocations={pickupLocations} />
        </Fragment>
      );
    }}
  />
);
export default PickupLocations;
