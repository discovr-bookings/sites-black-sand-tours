import Galleries from './Galleries';
import GalleryModal from './GalleryModal';
import ModalSlider from './ModalSlider';
import ModalShare from './ModalShare';

export { Galleries, GalleryModal, ModalSlider, ModalShare };
