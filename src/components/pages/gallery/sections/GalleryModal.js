import React from 'react';
import ReactModal from 'react-modal';

import { ModalSlider } from '.';

const ModalStyles = {
  overlay: {
    position: `fixed`,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    minHeight: `100%`,
    minWidth: `100%`,
    overflowY: `auto`,
    zIndex: `99999`,
    background: `#1a1e22`
  },
  content: {
    background: `transparent`,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    padding: 0,
    border: 0,
    borderRadius: 0
  }
};

ReactModal.setAppElement('#___gatsby');

const GalleryModal = props => {
  const {
    images,
    showModal,
    activeImage,
    activeGallery,
    companyInfo,
    closeModal
  } = props;
  return (
    <ReactModal
      isOpen={showModal}
      contentLabel="Image Gallery"
      onRequestClose={closeModal}
      shouldCloseOnOverlayClick
      style={ModalStyles}
    >
      <ModalSlider
        closeModal={closeModal}
        images={images}
        activeImage={activeImage}
        activeGallery={activeGallery}
        companyInfo={companyInfo}
      />
    </ReactModal>
  );
};

export default GalleryModal;
