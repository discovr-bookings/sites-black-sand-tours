import React, { Component } from 'react';
import Img from 'gatsby-image';
import styled from 'styled-components';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { GalleryModal } from '.';

const TabWrapper = styled(Tabs)``;

const TabTop = styled(TabList)`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px 0;
`;

const TabSelect = styled(Tab)`
  cursor: pointer;
  position: relative;
  margin: 0 20px 10px;
  padding: 0 2px 10px;
  outline: none;
  font-family: ${props => props.theme.font.header};
  line-height: 1.2em;
  text-transform: uppercase;
  display: inline-block;
  position: relative;
  font-size: 90%;
  color: ${props => props.theme.palette.text.black};
  letter-spacing: 0.25em;
  padding-bottom: 0.6em;
  &:hover {
    color: ${props => props.theme.palette.primary.main};
  }
  &:first-child {
    margin-right: 15px;
  }
  &:after {
    content: '';
    position: absolute;
    right: 0px;
    bottom: 0px;
    left: 0px;
    height: 1px;
    background-color: ${props => props.theme.palette.text.black};
  }
  &.react-tabs__tab--selected {
    color: ${props => props.theme.palette.primary.main};
  }
`;

const TabContent = styled(TabPanel)`
  && {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap: 15px;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      grid-template-columns: 1fr 1fr;
    }
    button {
      border: 0;
      border: none;
      padding: 0;
      outline: none;
    }
  }
`;

const TabImage = styled(Img)`
  && {
    cursor: pointer;
    height: 325px;
    @media screen and (max-width: ${props => props.theme.responsive.medium}) {
      height: 275px;
    }
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      height: 150px;
    }
    &:after {
      transition: all 1s ease-in-out;
      content: '';
      opacity: 0;
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 2;
      background: ${props => props.theme.palette.background.overlay};
    }
    img {
      transform: scale(1);
      transition: all 1s cubic-bezier(0.59, 0, 0.06, 1) 0s !important;
    }
    &:hover {
      &:after {
        opacity: 1;
      }
      img {
        transform: scale(1.1);
      }
    }
  }
`;

class Galleries extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeImage: null,
      activeGallery: null
    };
  }

  handleModalOpen = event => {
    const { openModal } = this.props;
    const imageindex = event.currentTarget.dataset.imageid;
    const galleryid = event.currentTarget.dataset.galleryid;
    this.setState({
      activeImage: imageindex,
      activeGallery: galleryid
    });
    openModal(event);
  };

  render() {
    const { activeImage, activeGallery } = this.state;
    const {
      images,
      showModal,
      galleryid,
      imageindex,
      companyInfo,
      closeModal
    } = this.props;

    return (
      <TabWrapper>
        <TabTop>
          {images.map(({ node: gallery }) => (
            <TabSelect key={gallery.id}>{gallery.title}</TabSelect>
          ))}
        </TabTop>
        {images.map(({ node: gallery }) => (
          <TabContent key={gallery.id}>
            {gallery.images.map((i, index) => (
              <button
                key={i.id}
                type="button"
                data-galleryid={gallery.title}
                data-imageid={index}
                onClick={this.handleModalOpen}
              >
                <TabImage fluid={i.fluid} alt={i.description} />
              </button>
            ))}
          </TabContent>
        ))}
        {showModal &&
          imageindex === activeImage &&
          galleryid === activeGallery && (
            <GalleryModal
              showModal={showModal}
              closeModal={closeModal}
              images={images}
              activeImage={activeImage}
              activeGallery={activeGallery}
              companyInfo={companyInfo}
            />
          )}
      </TabWrapper>
    );
  }
}

export default Galleries;
