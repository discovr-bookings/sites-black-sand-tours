import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  max-width: 800px;
  margin: 0 auto;
  h3 {
    font-size: 18px;
    margin-top: 50px;
    margin-bottom: 10px;
    padding-bottom: 10px;
    color: ${props => props.theme.palette.text.black};
    border-bottom: 1px solid ${props => props.theme.palette.border.dark};
    @media screen and (max-width: ${props => props.theme.responsive.medium}) {
      margin-top: 30px;
    }
  }
  ul {
    list-style: disc;
    padding-left: 20px;
    li {
      font-size: 14px;
      margin-bottom: 15px;
    }
  }
  p {
    font-size: 14px;
    a {
      color: ${props => props.theme.palette.primary.main};
      &:hover {
        color: ${props => props.theme.palette.primary.hover};
      }
    }
  }
`;

const LegalBody = props => {
  const { contentSection } = props;
  return (
    <Wrapper>
      <div
        dangerouslySetInnerHTML={{
          __html: contentSection[0].bodyType.childMarkdownRemark.html
        }}
      />
    </Wrapper>
  );
};

export default LegalBody;
