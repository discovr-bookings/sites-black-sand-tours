import React from 'react';
import styled from 'styled-components';
import {
  FacebookShareButton,
  TwitterShareButton,
  PinterestShareButton,
  EmailShareButton,
  WhatsappShareButton,
  FacebookIcon,
  TwitterIcon,
  PinterestIcon,
  EmailIcon,
  WhatsappIcon
} from 'react-share';
import config from '../../../../utils/siteConfig';

const Wrapper = styled.div`
  display: flex;
  margin-bottom: 1.5em;
  justify-content: left;
  div {
    margin-right: 5px;
    cursor: pointer;
    transition: all 300ms ease-in-out;
    &:hover {
      opacity: 0.9;
    }
  }
`;

const ShareIcons = props => {
  const { locationInfo, location } = props;
  const shareURl = config.siteUrl + location.pathname;
  const media = locationInfo.seoContent.featuredImage.fluid.src;
  const pageTitle = locationInfo.seoContent.pageTitle + '-' + config.siteTitle;
  return (
    <Wrapper>
      <FacebookShareButton url={shareURl} quote={pageTitle}>
        <FacebookIcon round size={40} />
      </FacebookShareButton>
      <TwitterShareButton url={shareURl}>
        <TwitterIcon round size={40} />
      </TwitterShareButton>
      <PinterestShareButton url={shareURl} media={media}>
        <PinterestIcon round size={40} />
      </PinterestShareButton>
      <WhatsappShareButton
        url={shareURl}
        title={pageTitle}
        separator=":: "
        className="share-button"
      >
        <WhatsappIcon size={40} round />
      </WhatsappShareButton>
      <EmailShareButton
        url={shareURl}
        subject={`Thought you may like this by ${pageTitle}`}
        body={shareURl}
      >
        <EmailIcon round size={40} />
      </EmailShareButton>
    </Wrapper>
  );
};

export default ShareIcons;
