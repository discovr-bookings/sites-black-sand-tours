import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: stretch;
  height: calc(100vh - 63px);
  flex-flow: row;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    display: block;
    width: 100%;
  }
`;

export default Container;
