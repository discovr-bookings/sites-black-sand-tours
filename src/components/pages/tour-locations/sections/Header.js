import React, { Component } from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';

import {
  LocationBackIcon,
  NextIcon
} from '../../../../assets/images/siteIcons';

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 63px;
  padding: 16px;
  background: ${props => props.theme.palette.secondary.main};
`;

const BackButton = styled(Link)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 14px;
  color: ${props => props.theme.palette.text.secondary};
  transition: color 0.2s ease-out 0s;
  svg {
    fill: ${props => props.theme.palette.text.secondary};
    width: 24px;
    height: 24px;
    margin-right: 10px;
  }
`;

const NextButton = styled(Link)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 14px;
  color: ${props => props.theme.palette.text.secondary};
  transition: color 0.2s ease-out 0s;
  svg {
    fill: ${props => props.theme.palette.text.secondary};
    width: 24px;
    height: 24px;
    margin-left: 10px;
  }
`;

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { pageContext } = this.props;
    return (
      <Wrapper>
        <BackButton to="/where-we-go/">
          {LocationBackIcon} Back to locations
        </BackButton>
        <NextButton to={`/where-we-go/${pageContext.next.seoContent.slug}/`}>
          {pageContext.next.seoContent.pageTitle}
          {NextIcon}
        </NextButton>
      </Wrapper>
    );
  }
}

export default Header;
