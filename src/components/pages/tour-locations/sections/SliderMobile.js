import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import Img from 'gatsby-image';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
  height: 60vh;
  overflow: hidden;
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    display: none;
  }
`;

const SliderImg = styled(Img)`
  && {
    height: 60vh;
    object-fit: cover;
  }
`;

class SliderMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { locationInfo } = this.props;

    return (
      <Wrapper>
        <Swiper>
          {locationInfo.imageGallery.map(i => (
            <div key={i.id}>
              <SliderImg
                fluid={i.fluid}
                alt={i.description}
                title={i.description}
              />
            </div>
          ))}
        </Swiper>
      </Wrapper>
    );
  }
}

export default SliderMobile;
