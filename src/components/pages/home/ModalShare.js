import React from 'react';
import styled from 'styled-components';
import {
  FacebookShareButton,
  TwitterShareButton,
  PinterestShareButton,
  EmailShareButton,
  FacebookIcon,
  TwitterIcon,
  PinterestIcon,
  EmailIcon
} from 'react-share';
import config from '../../../utils/siteConfig';

const Wrapper = styled.div`
  position: absolute;
  top: 45px;
  right: 137px;
  width: 44px;
  background: ${props => props.theme.palette.primary.main};
  z-index: 100;
`;

const ShareLinks = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  height: auto;
  > div {
    cursor: pointer;
    width: 100%;
    height: 44px;
    border-top: 1px solid rgba(0, 0, 0, 0.3);
    display: flex;
    justify-content: center;
    align-items: center;
    svg {
      height: 40px !important;
      width: 40px;
      rect {
        fill: transparent;
      }
      path {
        transition: fill 0.2s ease-in-out;
      }
    }
    &:hover {
      background: ${props => props.theme.palette.primary.hover};
    }
  }
`;

const ShareIcons = props => {
  const shareURl = config.siteUrl;
  const { images } = props;
  const media = images[0].fluid.src;
  return (
    <Wrapper className="shareButtons">
      <ShareLinks>
        <FacebookShareButton url={shareURl} quote={config.siteTitle}>
          <FacebookIcon size={40} />
        </FacebookShareButton>
        <TwitterShareButton url={shareURl}>
          <TwitterIcon size={40} />
        </TwitterShareButton>
        <PinterestShareButton url={shareURl} media={media}>
          <PinterestIcon size={40} />
        </PinterestShareButton>
        <EmailShareButton
          url={shareURl}
          subject={`Thought you may like this by ${config.siteTitle}`}
          body={shareURl}
        >
          <EmailIcon size={40} />
        </EmailShareButton>
      </ShareLinks>
    </Wrapper>
  );
};

export default ShareIcons;
