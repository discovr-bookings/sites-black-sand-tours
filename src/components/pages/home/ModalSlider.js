import React from 'react';
import styled from 'styled-components';
import Swiper from 'react-id-swiper';
import Img from 'gatsby-image';

import {
  ModalLeft,
  ModalRight,
  ModalClose,
  ContactIcon,
  EmailIcon,
  ShareIcon
} from '../../../assets/images/siteIcons';

import { ModalShare } from '.';

const Wrapper = styled.div``;

const ModalHeader = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 45px;
  z-index: 10001;
  display: flex;
  justify-content: flex-end;
  background: ${props => props.theme.palette.primary.main};
  border-bottom: 1px solid rgba(0, 0, 0, 0.3);
`;

const TourTitle = styled.h3`
  position: absolute;
  top: 0;
  left: 15px;
  z-index: 999998;
  letter-spacing: 3px;
  text-transform: uppercase;
  font-size: 15px;
  line-height: 45px;
  font-family: ${props => props.theme.font.header};
  color: ${props => props.theme.palette.text.secondary};
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    top: 75px;
    left: 0;
    width: 100%;
    line-height: 1.5;
    text-align: center;
    z-index: 9;
  }
`;

const BookButton = styled.a`
  position: absolute;
  letter-spacing: 3px;
  top: 0;
  left: 50%;
  background: ${props => props.theme.palette.primary.main};
  transform: translateX(-50%);
  width: auto;
  padding: 0 34px;
  border-left: 1px solid rgba(0, 0, 0, 0.3);
  border-right: 1px solid rgba(0, 0, 0, 0.3);
  border-bottom: 1px solid rgba(0, 0, 0, 0.3);
  cursor: pointer;
  text-transform: uppercase;
  font-size: 15px;
  line-height: 44px;
  font-family: ${props => props.theme.font.header};
  color: ${props => props.theme.palette.text.secondary};
  z-index: 999998;
  transition: all 300ms ease-in-out;
  text-decoration: none;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    left: 0;
    transform: translateX(0);
    border-left: none;
  }
  &:hover {
    background: ${props => props.theme.palette.primary.hover};
    color: ${props => props.theme.palette.text.secondary};
  }
  &:focus {
    background: ${props => props.theme.palette.primary.hover};
    color: ${props => props.theme.palette.text.secondary};
  }
`;

const Button = styled.a`
  height: 45px;
  width: 45px;
  padding: 8px;
  border-left: 1px solid rgba(0, 0, 0, 0.3);
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 999999;
  &:hover {
    background: ${props => props.theme.palette.primary.hover};
    &.shareIcon {
      border-bottom: 1px solid rgba(0, 0, 0, 0.3);
    }
    .shareButtons {
      @media screen and (min-width: ${props => props.theme.responsive.medium}) {
        transform: scaleY(1);
        opacity: 1;
        visibility: visible;
      }
    }
  }
  &.emailIcon {
    svg {
      fill: ${props => props.theme.palette.text.secondary};
    }
  }
  &.shareIcon {
    svg {
      height: 19px;
      path {
        fill: ${props => props.theme.palette.text.secondary};
      }
    }
  }
  &.phoneIcon {
    svg {
      fill: none;
      stroke-width: 2;
      stroke: ${props => props.theme.palette.text.secondary};
    }
  }
  &.showShare {
    .shareButtons {
      @media screen and (max-width: ${props => props.theme.responsive.medium}) {
        transform: scaleY(1);
        opacity: 1;
        visibility: visible;
      }
    }
  }
  .shareButtons {
    transform: scaleY(0);
    transform-origin: left top 0px;
    opacity: 0;
    visibility: hidden;
    transition: all 300ms ease-in-out;
  }
  svg {
    width: 25px;
    height: 25px;
    transition: all 300ms ease-in-out;
  }
`;

const CloseIcon = styled.div`
  padding: 8px;
  border-left: 1px solid rgba(0, 0, 0, 0.3);
  height: 45px;
  cursor: pointer;
  &:hover {
    background: ${props => props.theme.palette.primary.hover};
    svg {
      transform: rotate(180deg);
    }
  }
  svg {
    fill: none;
    stroke-width: 2;
    stroke: ${props => props.theme.palette.text.secondary};
    width: 30px;
    height: 30px;
    transition: all 300ms ease-in-out;
  }
`;

const SliderWrapper = styled.div`
  bottom: 0;
  height: 100%;
  left: 0;
  position: fixed;
  right: 0;
  top: 0;
  width: 100%;
  z-index: 5;
  max-height: 500px;
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    top: 80px;
    height: calc(100% - 160px);
  }
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    top: 50%;
    transform: translateY(-50%);
    height: calc(100% - 40%);
  }
  .swiper-container {
    &:first-child {
      position: relative;
      line-height: 0;
      height: 100%;
      overflow: visible;
      .swiper-wrapper {
        .swiper-slide {
          .gatsby-image-wrapper {
            height: 100%;
            width: 100%;
            border: 1px solid #1a1e22;
            @media screen and (min-width: ${props =>
                props.theme.responsive.medium}) {
              width: calc(100% - 200px);
              max-width: 800px;
              margin: 0 auto;
            }
          }
        }
      }
    }
    &:last-child {
      margin: 10px auto;
      max-width: 800px;
      .swiper-wrapper {
        .swiper-slide {
          width: 50px !important;
          height: 50px;
          border-radius: 3px;
          opacity: 0.4;
          cursor: pointer;
          transition: all 300ms ease-in-out;
          &:hover {
            opacity: 1;
            &.gatsby-image-wrapper {
              img {
                transform: scale(1.1);
              }
            }
          }
          &.swiper-slide-active {
            opacity: 1;
          }
          &.gatsby-image-wrapper {
            img {
              transition: all 1s cubic-bezier(0.59, 0, 0.06, 1) 0s !important;
            }
          }
        }
      }
    }
  }
`;

const NavButton = styled.div`
  top: 50%;
  transform: translateY(-50%);
  display: none;
  padding: 0;
  width: 44px !important;
  height: 44px !important;
  background-image: none !important;
  text-align: center;
  background: rgba(0, 0, 0, 0.2);
  color: #${props => props.theme.palette.text.secondary};
  border: 2px solid ${props => props.theme.palette.background.light};
  border-radius: 50%;
  margin: 0 8px;
  transition: all 300ms ease-in-out;
  cursor: pointer;
  outline: none;
  position: absolute;
  z-index: 4;
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    display: block;
  }
  @media screen and (min-width: ${props => props.theme.responsive.large}) {
    margin: 0 40px;
  }
  &:hover {
    background: rgba(0, 0, 0, 0.5);
  }
  &.swiper-button-next {
    right: 0;
  }
  &.swiper-button-previous {
    left: 0;
  }
  span {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    svg {
      fill: none;
      stroke-width: 2;
      stroke: ${props => props.theme.palette.text.secondary};
      width: 24px;
      height: 24px;
    }
  }
`;

class ModalSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gallerySwiper: null,
      thumbnailSwiper: null,
      shareOpen: false
    };
    this.handleToggleShare = this.handleToggleShare.bind(this);
    this.galleryRef = this.galleryRef.bind(this);
    this.thumbRef = this.thumbRef.bind(this);
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextState.gallerySwiper && nextState.thumbnailSwiper) {
      const { gallerySwiper, thumbnailSwiper } = nextState;

      gallerySwiper.controller.control = thumbnailSwiper;
      thumbnailSwiper.controller.control = gallerySwiper;
    }
  }

  handleToggleShare() {
    this.setState(state => ({ shareOpen: !state.shareOpen }));
  }

  galleryRef(ref) {
    if (ref) this.setState({ gallerySwiper: ref.swiper });
  }

  thumbRef(ref) {
    if (ref) this.setState({ thumbnailSwiper: ref.swiper });
  }

  render() {
    const {
      activeImage,
      activeGallery,
      companyInfo,
      images,
      closeModal
    } = this.props;

    const { email, phone, rezdyBookingLink } = companyInfo;

    const { shareOpen } = this.state;

    const gallerySwiperParams = {
      spaceBetween: 10,
      effect: 'fade',
      initialSlide: parseFloat(activeImage),
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      renderPrevButton: () => (
        <NavButton className="swiper-button-prev">
          <span>{ModalLeft}</span>
        </NavButton>
      ),
      renderNextButton: () => (
        <NavButton className="swiper-button-next">
          <span>{ModalRight}</span>
        </NavButton>
      )
    };

    const thumbnailSwiperParams = {
      spaceBetween: 10,
      initialSlide: parseFloat(activeImage),
      centeredSlides: true,
      slidesPerView: 'auto',
      touchRatio: 0.2,
      slideToClickedSlide: true
    };

    return (
      <Wrapper>
        <TourTitle>{activeGallery} Gallery</TourTitle>
        <BookButton
          href={rezdyBookingLink}
          target="_blank"
          rel="noopener noreferrer"
          title="Book your Black Sand Adventures Tour"
        >
          Book The Tour
        </BookButton>
        <ModalHeader>
          <Button
            onClick={this.handleToggleShare}
            className={`shareIcon ${!shareOpen ? '' : 'showShare'}`}
            href="javascript:;"
          >
            <ShareIcon />
            <ModalShare images={images} galleryid={activeGallery} />
          </Button>
          <Button className="phoneIcon" href={`tel:${phone}`}>
            {ContactIcon}
          </Button>
          <Button className="emailIcon" href={`mailto:${email}`}>
            {EmailIcon}
          </Button>
          <CloseIcon onClick={closeModal}>{ModalClose}</CloseIcon>
        </ModalHeader>
        <SliderWrapper>
          <Swiper {...gallerySwiperParams} ref={this.galleryRef}>
            {images.map(i => (
              <div key={i.id}>
                <Img
                  fluid={i.fluid}
                  alt={i.description}
                  title={i.description}
                />
              </div>
            ))}
          </Swiper>
          <Swiper {...thumbnailSwiperParams} ref={this.thumbRef}>
            {images.map(i => (
              <Img
                key={i.id}
                fluid={i.fluid}
                alt={i.description}
                title={i.description}
              />
            ))}
          </Swiper>
        </SliderWrapper>
      </Wrapper>
    );
  }
}

export default ModalSlider;
