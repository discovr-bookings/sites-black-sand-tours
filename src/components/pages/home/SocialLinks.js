import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

import config from '../../../utils/siteConfig';
import {
  FacebookIcon,
  InstagramIcon,
  GoogleIcon,
  TripAdvisorIcon
} from '../../../assets/images/siteIcons';

const Wrapper = styled.ul`
  display: flex;
  align-items: center;
  margin-top: 1em;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    height: 60px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-top: 1.5em;
    justify-content: center;
  }
`;

const Item = styled.li`
  &:not(:last-child) {
    margin-right: 1em;
  }
  &:last-child {
    svg {
      width: 42px;
    }
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    text-align: center;
    font-size: 25px;
    margin-right: 20px;
  }
  svg {
    width: 30px;
    height: 30px;
    path {
      transition: all 300ms ease-in-out;
      fill: ${props => props.theme.palette.text.secondary};
    }
  }
  &:hover {
    svg {
      path {
        fill: ${props => props.theme.palette.primary.main};
      }
    }
  }
`;

const SocialLinks = () => (
  <StaticQuery
    query={graphql`
      query {
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          facebook
          instagram
          google
          tripAdvisorUrl
        }
      }
    `}
    render={data => {
      const {
        facebook,
        instagram,
        google,
        tripAdvisorUrl
      } = data.contentfulCompanyInformation;
      return (
        <Wrapper>
          <Item>
            <a
              href={facebook}
              target="_blank"
              rel="noopener noreferrer"
              title={`${config.siteTitle} Facebook`}
            >
              <FacebookIcon />
            </a>
          </Item>
          <Item>
            <a
              href={instagram}
              target="_blank"
              rel="noopener noreferrer"
              title={`${config.siteTitle} Instagram`}
            >
              <InstagramIcon />
            </a>
          </Item>
          <Item>
            <a
              href={google}
              target="_blank"
              rel="noopener noreferrer"
              title={`${config.siteTitle} Google Photos`}
            >
              <GoogleIcon />
            </a>
          </Item>
          <Item>
            <a
              href={tripAdvisorUrl}
              title={`${config.siteTitle} TripAdvisor Reviews`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <TripAdvisorIcon />
            </a>
          </Item>
        </Wrapper>
      );
    }}
  />
);

export default SocialLinks;
