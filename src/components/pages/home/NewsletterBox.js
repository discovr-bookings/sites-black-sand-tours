import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';
import Img from 'gatsby-image';
import { MailChimp } from '.';

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 30px;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    grid-template-columns: 1fr;
  }
  a {
    display: grid;
    grid-template-columns: 1fr;
    grid-gap: 20px;
    &:last-child {
      @media screen and (max-width: ${props => props.theme.responsive.small}) {
        margin-top: 50px;
      }
    }
    img {
      transform: scale(1);
      transition: all 1s cubic-bezier(0.59, 0, 0.06, 1) 0s !important;
    }
    p {
      position: relative;
      display: inline-block;
      margin: 0 auto;
      font-family: ${props => props.theme.font.header};
      line-height: 1.2em;
      text-transform: uppercase;
      font-size: 0.85em;
      color: ${props => props.theme.palette.text.black};
      letter-spacing: 0.25em;
      padding-bottom: 0.6em;
      transition: all 300ms ease-in-out;
      &:after {
        content: '';
        position: absolute;
        right: 0px;
        bottom: 0px;
        left: 0px;
        height: 1px;
        background-color: ${props => props.theme.palette.text.black};
      }
    }
    &:hover {
      img {
        transform: scale(1.1);
      }
      p {
        color: ${props => props.theme.palette.primary.main};
      }
    }
  }
`;

const NewsletterBox = props => {
  const { contentSection } = props;
  return (
    <StaticQuery
      query={graphql`
        query {
          contentfulCompanyInformation(
            id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
          ) {
            rezdyBookingLink
          }
        }
      `}
      render={data => {
        const { rezdyBookingLink } = data.contentfulCompanyInformation;
        return (
          <Wrapper>
            <Link
              to={`${contentSection[2].link.seoContent.slug}/`}
              title={contentSection[2].link.seoContent.pageTitle}
            >
              <Img
                fluid={contentSection[2].image.fluid}
                title={contentSection[2].image.title}
                alt={contentSection[2].image.description}
              />
              <p>{contentSection[2].link.seoContent.pageTitle}</p>
            </Link>
            <MailChimp />
            <a
              href={rezdyBookingLink}
              target="_blank"
              rel="noopener noreferrer"
              title="Book your Black Sand Adventures Tour"
            >
              <Img
                fluid={contentSection[3].image.fluid}
                title={contentSection[3].image.title}
                alt={contentSection[3].image.description}
              />
              <p>Book the tour</p>
            </a>
          </Wrapper>
        );
      }}
    />
  );
};

export default NewsletterBox;
