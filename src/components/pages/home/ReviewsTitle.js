import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  text-align: center;
  position: relative;
`;

const HashTag = styled.h5`
  text-align: center;
`;

const Title = styled.h2`
  text-align: center;
`;

const ReviewsTitle = () => (
  <Wrapper>
    <HashTag>#travellikealocal</HashTag>
    <Title>Enjoyed by everyone</Title>
  </Wrapper>
);

export default ReviewsTitle;
