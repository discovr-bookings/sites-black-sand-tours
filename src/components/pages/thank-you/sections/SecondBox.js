import React from 'react';
import styled from 'styled-components';
import { FacebookShareButton } from 'react-share';
import {
  FacebookIcon,
  InstagramIcon
} from '../../../../assets/images/siteIcons';

const Wrapper = styled.div`
  padding-left: 30px;
  position: relative;
  padding-bottom: 40px;
  text-align: left;
`;

const Inner = styled.div`
  width: 91.67%;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    width: 100%;
  }
`;

const Layout = styled.div`
  list-style: none;
  margin-left: -20px;
  padding: 0px;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-left: -20px;
  }
  &:after {
    z-index: 10;
    position: absolute;
    content: '';
    height: 2px;
    background-color: rgb(204, 204, 204);
    top: 14px;
    width: 100%;
    left: 30px;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      left: 35px;
      top: 14px;
      width: 2px;
      height: 100%;
    }
  }
`;

const Container = styled.div`
  display: inline-block;
  padding-left: 24px;
  vertical-align: top;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    padding-left: 20px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding-left: 10px;
    &:first-child {
      width: 15%;
    }
    &:last-child {
      width: 85%;
    }
  }
  .facebook-button {
    background: #4266b2;
    color: #fff;
    cursor: pointer;
    font-family: Helvetica, Arial, sans-serif;
    margin: 0;
    white-space: nowrap;
    padding: 5px 15px;
    font-weight: 700;
    font-size: 13px;
    border-radius: 4px;
    width: 100px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    outline: none;
    &:hover {
      color: #fff;
      opacity: 0.9;
    }
    svg {
      width: 15px;
      height: 15px;
      path {
        fill: #fff;
      }
    }
  }
  .instagram {
    background: radial-gradient(
        circle farthest-corner at 35% 90%,
        #fec564,
        transparent 50%
      ),
      radial-gradient(
        circle farthest-corner at 0 140%,
        #fec564,
        transparent 50%
      ),
      radial-gradient(
        ellipse farthest-corner at 0 -25%,
        #5258cf,
        transparent 50%
      ),
      radial-gradient(
        ellipse farthest-corner at 20% -50%,
        #5258cf,
        transparent 50%
      ),
      radial-gradient(
        ellipse farthest-corner at 100% 0,
        #893dc2,
        transparent 50%
      ),
      radial-gradient(
        ellipse farthest-corner at 60% -20%,
        #893dc2,
        transparent 50%
      ),
      radial-gradient(
        ellipse farthest-corner at 100% 100%,
        #d9317a,
        transparent
      ),
      linear-gradient(
        #6559ca,
        #bc318f 30%,
        #e33f5f 50%,
        #f77638 70%,
        #fec66d 100%
      );
    color: #fff;
    outline: none;
    cursor: pointer;
    font-family: Helvetica, Arial, sans-serif;
    margin: 0;
    white-space: nowrap;
    padding: 5px 15px;
    font-weight: 700;
    margin-left: 10px;
    font-size: 13px;
    border-radius: 4px;
    width: 100px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    &:hover {
      color: #fff;
      opacity: 0.9;
    }
    svg {
      width: 15px;
      height: 15px;
      path {
        fill: #fff;
      }
    }
  }
`;

const Step = styled.div`
  margin-bottom: 12px;
`;

const Number = styled.div`
  line-height: 30px;
  background-color: rgb(255, 255, 255);
  color: rgb(204, 204, 204);
  height: 32px;
  position: relative;
  text-align: center;
  top: -2px;
  width: 32px;
  z-index: 20;
  border-color: rgb(204, 204, 204);
  border-style: solid;
  border-radius: 50%;
  border-width: 2px;
`;

const SecondBox = () => (
  <Wrapper>
    <Inner>
      <Layout>
        <Container>
          <Step>
            <Number>2</Number>
          </Step>
        </Container>
        <Container>
          <h4>Would you like to stay in touch with Black Sand Tours?</h4>
          <p>
            Hope you had as much fun as we did. Follow us on social media and
            see what we are up to in the future.
          </p>
          <div style={{ display: `flex` }}>
            <FacebookShareButton
              url="https://www.blacksandtours.com"
              quote="Auckland's most adventurous day trip to black sand beaches including Piha, Muriwai, Bethells and Kitekite Falls - spend the day hiking and swimming in beautiful waterfalls, caves, sand dunes with us."
              className="facebook-button"
            >
              Share <FacebookIcon />
            </FacebookShareButton>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.instagram.com/blacksandtours/"
              className="instagram"
            >
              Follow <InstagramIcon />
            </a>
          </div>
        </Container>
      </Layout>
    </Inner>
  </Wrapper>
);

export default SecondBox;
