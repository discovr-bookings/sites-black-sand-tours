import React from 'react';
import styled from 'styled-components';
import MailchimpSubscribe from 'react-mailchimp-subscribe';

const Wrapper = styled.div`
  text-align: center;
  input {
    outline: none;
    font-family: ${props => props.theme.font.header};
    background: ${props => props.theme.palette.background.light};
    box-sizing: border-box;
    width: 100%;
    margin-bottom: 15px;
    border: 1px solid ${props => props.theme.palette.border.dark};
    font-size: 14px;
    line-height: 20px;
    letter-spacing: 1px;
    color: ${props => props.theme.palette.text.primary};
    padding: 14px;
    resize: none;
    -webkit-appearance: none;
    border-radius: 0;
    transition: all 300ms ease-in-out;
    &:hover,
    &:focus {
      border: 1px solid ${props => props.theme.palette.primary.main};
    }
  }
  label {
    font-family: ${props => props.theme.font.header};
    color: ${props => props.theme.palette.text.grey};
    font-size: 14px;
    opacity: 0;
    visibility: hidden;
    height: 0;
    width: 0;
    position: absolute;
    z-index: -1;
  }
  button {
    width: 100%;
    outline: none;
    background: ${props => props.theme.palette.background.dark};
    color: ${props => props.theme.palette.text.secondary};
    border: none;
    margin-bottom: 15px;
    padding: 14px;
    font-size: 14px;
    font-weight: 700;
    line-height: 20px;
    line-height: 20px;
    cursor: pointer;
  }
`;

const CustomForm = ({ status, message, onValidated }) => {
  let email;
  let name;
  const submit = () =>
    email &&
    name &&
    email.value.indexOf('@') > -1 &&
    onValidated({
      EMAIL: email.value,
      NAME: name.value
    });

  return (
    <Wrapper>
      <label htmlFor="name">Name</label>
      <input
        id="name"
        name="name"
        ref={node => (name = node)}
        type="text"
        placeholder="Your Name"
      />
      <label htmlFor="email">Email</label>
      <input
        id="email"
        name="email"
        ref={node => (email = node)}
        type="email"
        placeholder="Your email"
      />
      <br />
      <button onClick={submit} type="button">
        Submit
      </button>
      {status === 'sending' && <div style={{ color: 'blue' }}>sending...</div>}
      {status === 'error' && (
        <div
          style={{ color: 'red' }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
      )}
      {status === 'success' && (
        <div
          style={{ color: 'green' }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
      )}
    </Wrapper>
  );
};

const url =
  'https://blacksandtours.us12.list-manage.com/subscribe/post?u=9ba7c2a53ea0d449d14eba316&amp;id=278d825e33';

const MailChimp = () => (
  <MailchimpSubscribe
    url={url}
    render={({ subscribe, status, message }) => (
      <CustomForm
        status={status}
        message={message}
        onValidated={formData => subscribe(formData)}
      />
    )}
  />
);

export default MailChimp;
