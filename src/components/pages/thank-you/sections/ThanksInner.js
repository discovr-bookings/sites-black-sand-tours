import styled from 'styled-components';

const ThanksInner = styled.div`
  width: 90%;
  margin: 0 auto;
  overflow: hidden;
  border-radius: 6px;
  background: ${props => props.theme.palette.background.light};
  padding: 2em 42px;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    padding-right: 21px;
    padding-left: 21px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    border-radius: 0;
    width: 100%;
  }
`;

export default ThanksInner;
