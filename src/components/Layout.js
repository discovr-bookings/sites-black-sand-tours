import React, { Fragment } from 'react';
import { ThemeProvider } from 'styled-components';

import GlobalStyle from '../assets/styles/global';
import Theme from '../assets/styles/theme';
import Header from './global/header';
import Freezer from './global/freezer';
import Footer from './global/footer';

import {
  Container,
  InnerContainer,
  SeoDetails,
  PageContainer
} from './global/templates';
import Menu from './global/menu';

class Template extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scrolling: false,
      menuModal: false,
      sharing: false
    };
  }

  innerContainer = null;

  componentDidMount() {
    this.innerContainer.addEventListener('scroll', this.handleScroll);
    this.handleScroll();
  }

  componentWillUnmount() {
    this.innerContainer.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    const { scrolling } = this.state;
    if (this.innerContainer.scrollTop === 0 && scrolling === true) {
      this.setState({ scrolling: false });
    } else if (this.innerContainer.scrollTop !== 0 && scrolling !== true) {
      this.setState({ scrolling: true });
    }
  };

  handleToggleMenuAnimate = () => {
    this.setState(state => ({
      menuAnimate: !state.menuAnimate
    }));
  };

  handleToggleMenuModal = () => {
    this.setState(state => ({
      menuModal: !state.menuModal
    }));
  };

  handleToggleMenuClick = () => {
    this.handleToggleMenuAnimate();
    setTimeout(() => this.handleToggleMenuModal(), 400);
  };

  handleToggleShareClick = () => {
    this.setState(state => ({
      sharing: !state.sharing
    }));
  };

  render() {
    const { children, seoContent, whereToPage } = this.props;
    const { menuModal, menuAnimate, scrolling, sharing } = this.state;
    return (
      <ThemeProvider theme={Theme}>
        <Fragment>
          <SeoDetails seoContent={seoContent} menuOpen={menuModal} />
          <Container className={menuAnimate ? 'active' : ''}>
            {!whereToPage ? (
              <Header
                scrolling={scrolling}
                seoContent={seoContent}
                menuClick={this.handleToggleMenuClick}
                sharing={sharing}
                shareClick={this.handleToggleShareClick}
              />
            ) : null}
            <InnerContainer ref={el => (this.innerContainer = el)}>
              <PageContainer>
                {children}
                {!whereToPage ? <Footer /> : null}
              </PageContainer>
            </InnerContainer>
            <Freezer
              menuOpen={menuAnimate}
              menuClick={this.handleToggleMenuClick}
            />
          </Container>
          <Menu menuOpen={menuModal} />
          <GlobalStyle />
        </Fragment>
      </ThemeProvider>
    );
  }
}

export default Template;
