import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import SubMenu from '../components/global/subMenu';
import {
  Box,
  BoxInner,
  BoxTop,
  FullWidth,
  BoxBottom
} from '../components/global/templates';
import { TourDetails, TourTabs } from '../components/pages/what-we-do';

class WhatWeDoPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent, contentSection } = data.contentfulPages;
    const tabContentSection = [
      contentSection[1],
      contentSection[2],
      contentSection[3],
      contentSection[4]
    ];
    const menu = data.contentfulMenus;
    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero location={location} seoContent={seoContent} />
        <SubMenu menu={menu} />
        <Box noBG FirstBox>
          <FullWidth isSlider>
            <BoxTop isSlider reversed contentSection={contentSection[0]} />
          </FullWidth>
        </Box>
        <Box Grey>
          <BoxInner Grey>
            <TourTabs tabContentSection={tabContentSection} />
          </BoxInner>
        </Box>
        <Box Primary>
          <BoxInner Primary>
            <TourDetails contentSection={contentSection[5]} />
          </BoxInner>
        </Box>
        <Box TwoRows>
          <FullWidth>
            <BoxTop contentSection={contentSection[6]} />
            <BoxBottom contentSection={contentSection[7]} />
          </FullWidth>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "2ae112be-508b-565f-ba8a-36ed4c66776c" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 2000) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
      contentSection {
        name
        bodyType {
          bodyType
          childMarkdownRemark {
            html
          }
        }
        image {
          title
          description
          fluid(maxWidth: 1200) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
        imageGallery {
          id
          title
          description
          file {
            url
          }
          fluid(maxWidth: 800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
        link {
          seoContent {
            pageTitle
            slug
          }
        }
        jsonData {
          id
          title
          linksHeader
          text
          type {
            fact
            name
          }
          links {
            icon
            link
            linkText
            text
            title
          }
        }
      }
    }
    contentfulMenus(id: { eq: "6b4d21fc-dc89-5cdd-975b-cb31ceabea7a" }) {
      id
      page {
        id
        seoContent {
          pageTitle
          slug
        }
      }
    }
  }
`;

export default WhatWeDoPage;
