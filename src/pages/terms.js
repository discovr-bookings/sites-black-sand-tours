import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import SubMenu from '../components/global/subMenu';
import { Box, BoxInner } from '../components/global/templates';
import LegalBody from '../components/pages/legal';

class TermsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent, contentSection } = data.contentfulPages;
    const menu = data.contentfulMenus;

    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero location={location} seoContent={seoContent} />
        <SubMenu menu={menu} />
        <Box>
          <BoxInner>
            <LegalBody contentSection={contentSection} />
          </BoxInner>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "ee00399d-8203-56f8-9bc4-a1e1c93704b9" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 1800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
      contentSection {
        bodyType {
          childMarkdownRemark {
            html
          }
        }
      }
    }
    contentfulMenus(id: { eq: "437431f2-c91e-5f42-97e3-79e6c76b0695" }) {
      id
      page {
        id
        seoContent {
          pageTitle
          slug
        }
      }
    }
  }
`;

export default TermsPage;
