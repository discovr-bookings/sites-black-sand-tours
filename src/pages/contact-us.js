import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import SubMenu from '../components/global/subMenu';
import { Box, BoxInner } from '../components/global/templates';
import ContactUs from '../components/pages/contact';

class ContactPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent } = data.contentfulPages;
    const menu = data.contentfulMenus;
    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero location={location} seoContent={seoContent} />
        <SubMenu menu={menu} />
        <Box>
          <BoxInner>
            <ContactUs />
          </BoxInner>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "69912f23-90f3-5557-b710-1ebf4a6de5c1" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 2000) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
    }
    contentfulMenus(id: { eq: "0946fedb-57af-562f-b244-07b4eba3d482" }) {
      id
      page {
        id
        seoContent {
          pageTitle
          slug
        }
      }
    }
  }
`;

export default ContactPage;
