import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import SubMenu from '../components/global/subMenu';
import { Box, BoxInner, PageTitle } from '../components/global/templates';
import Reviews from '../components/pages/reviews';

class ReviewsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent } = data.contentfulPages;
    const menu = data.contentfulMenus;
    const { tripAdvisorId } = data.contentfulCompanyInformation;
    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero location={location} seoContent={seoContent} />
        <SubMenu menu={menu} />
        <Box>
          <BoxInner>
            <PageTitle location={location} titleContent={seoContent} />
            <Reviews tripAdvisorId={tripAdvisorId} />
          </BoxInner>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "0ed922ed-bd96-521b-b031-a6a23dd94cd2" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 2000) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
    }
    contentfulMenus(id: { eq: "6b4d21fc-dc89-5cdd-975b-cb31ceabea7a" }) {
      id
      page {
        id
        seoContent {
          pageTitle
          slug
        }
      }
    }
    contentfulCompanyInformation(
      id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
    ) {
      tripAdvisorId
    }
  }
`;

export default ReviewsPage;
