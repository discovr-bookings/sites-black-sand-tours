import React from 'react';
import { graphql, Link } from 'gatsby';
import styled from 'styled-components';

import Layout from '../components/Layout';
import gifbg from '../assets/images/giphy.gif';

const Wrapper = styled.div`
  height: 100vh;
`;

const Text = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  flex-direction: column;
  top: 0;
  left: 0;
  padding: 2em;
  height: 100vh;
  width: 100%;
  z-index: 6;
  h1,
  p {
    color: #fff;
    text-align: center;
  }
  a {
    width: auto;
    outline: none;
    background: ${props => props.theme.palette.primary.main};
    color: ${props => props.theme.palette.text.secondary};
    border: none;
    margin-bottom: 15px;
    padding: 14px;
    font-size: 14px;
    font-weight: 700;
    line-height: 20px;
    line-height: 20px;
    cursor: pointer;
    &:hover {
      background: ${props => props.theme.palette.primary.hover};
      color: ${props => props.theme.palette.text.secondary};
    }
  }
`;

const Image = styled.div`
  height: 100vh;
  position: absolute;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  background: url(${gifbg});
  background-size: cover;
  background-position: center;
  z-index: 1;
  &:after {
    content: '';
    position: absolute;
    background: rgba(0, 0, 0, 0.7);
    top: 0;
    left: 0;
    height: 100vh;
    width: 100%;
  }
`;

class NotFoundPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent } = data.contentfulPages;

    return (
      <Layout location={location} seoContent={seoContent}>
        <Wrapper>
          <Image />
          <Text>
            <h1>Page not found</h1>
            <p>{location.href} doesn&apos;t exist or has been moved.</p>
            <Link to="/">Back to Home Page</Link>
          </Text>
        </Wrapper>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "53bfebc1-61a1-5fbe-bf42-bdbd02055db7" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 1800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
      contentSection {
        bodyType {
          childMarkdownRemark {
            html
          }
        }
      }
    }
  }
`;

export default NotFoundPage;
