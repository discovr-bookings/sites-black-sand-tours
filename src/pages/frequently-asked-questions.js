import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import SubMenu from '../components/global/subMenu';
import { Box, BoxInner, PageTitle } from '../components/global/templates';
import FAQ from '../components/pages/faq';

class FaqPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent } = data.contentfulPages;
    const menu = data.contentfulMenus;
    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero location={location} seoContent={seoContent} />
        <SubMenu menu={menu} />
        <Box>
          <BoxInner>
            <PageTitle location={location} titleContent={seoContent} />
            <FAQ />
          </BoxInner>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "7a421e54-44fc-5d2c-bb50-9404632cca8f" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 2000) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
    }
    contentfulMenus(id: { eq: "6b4d21fc-dc89-5cdd-975b-cb31ceabea7a" }) {
      id
      page {
        id
        seoContent {
          pageTitle
          slug
        }
      }
    }
  }
`;

export default FaqPage;
