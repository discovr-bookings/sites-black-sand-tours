import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import SubMenu from '../components/global/subMenu';
import { Box, BoxInner, PageTitle } from '../components/global/templates';
import WhereWeGoItems from '../components/pages/where-we-go';

class WhereWeGoPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent } = data.contentfulPages;
    const menu = data.contentfulMenus;
    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero location={location} seoContent={seoContent} />
        <SubMenu menu={menu} />
        <Box>
          <BoxInner>
            <PageTitle location={location} titleContent={seoContent} />
            <WhereWeGoItems />
          </BoxInner>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "35c1a752-8e21-5f24-a762-60981decdcc1" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
          childMarkdownRemark {
            html
          }
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 2000) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
    }
    contentfulMenus(id: { eq: "6b4d21fc-dc89-5cdd-975b-cb31ceabea7a" }) {
      id
      page {
        id
        seoContent {
          pageTitle
          slug
        }
      }
    }
  }
`;

export default WhereWeGoPage;
