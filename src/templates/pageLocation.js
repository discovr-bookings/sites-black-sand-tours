import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import TourLocations from '../components/pages/tour-locations';

class TourLocationsTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location, pageContext } = this.props;
    const { seoContent } = data.contentfulTourLocations;
    return (
      <Layout whereToPage location={location} seoContent={seoContent}>
        <TourLocations
          locationInfo={data.contentfulTourLocations}
          location={location}
          pageContext={pageContext}
        />
      </Layout>
    );
  }
}

export const query = graphql`
  query locationQuery($slug: String!) {
    contentfulTourLocations(seoContent: { slug: { eq: $slug } }) {
      id
      name
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 2000) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
      body {
        childMarkdownRemark {
          html
        }
      }
      imageGallery {
        id
        title
        description
        fluid(maxWidth: 1500) {
          ...GatsbyContentfulFluid_withWebp_noBase64
        }
      }
    }
  }
`;

export default TourLocationsTemplate;
