const config = {
  siteTitle: 'Blacksand Adventure Tours - Auckland, New Zealand',
  siteDescription:
    'The most fun adventure day-trip in Auckland, and the very best way to experience Piha Beach, Muriwai, Whatipu, Bethells Beach and Kitekite Falls',
  siteUrl: 'https://www.blacksandtours.com'
};

export default config;
