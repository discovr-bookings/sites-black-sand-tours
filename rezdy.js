import moment from 'moment';

// Search Pickup list
export const pickuplist = [
  {
    uri: '/v1/pickups',
    searchString: 'central_auckland_pickups',
    api_key: '8d38b04957dd4a1eadfe2c4414a9ec2d'
  }
];

// Search Product id
export const productID = [
  {
    uri: '/v1/products',
    api_key: '8d38b04957dd4a1eadfe2c4414a9ec2d',
    filter: [
      {
        productCode: 'PWJRDQ'
      },
      {
        productCode: 'PSF1XT'
      }
    ]
  }
];

// Search all bookings
export const bookings = [
  {
    uri: '/v1/bookings',
    api_key: '8d38b04957dd4a1eadfe2c4414a9ec2d',
    filter: [
      {
        firstName: '' // return orderNumber
      },
      {
        lastName: '' // return orderNumber
      }
    ]
  }
];

// Put booking
export const updateBookings = [
  {
    uri: '/v1/bookings',
    orderNumber: bookings,
    api_key: '8d38b04957dd4a1eadfe2c4414a9ec2d',
    body: [
      {
        dateUpdated: moment().format('YYYY-MM-DDTdd:mm:ssZ') // return date-time 2019-01-15T14:02:14Z
      },
      {
        items: [
          {
            pickupLocation: {
              locationName: pickuplist
            }
          }
        ]
      }
    ]
  }
];
